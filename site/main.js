import BtnCellRenderer from './btn-cell-renderer.js';

const columnDefs = [
    { field: 'fr_gloss', filter: 'agTextColumnFilter', headerName: 'French Gloss'},
    { field: 'en_gloss', filter: 'agTextColumnFilter', headerName: 'English Gloss'},

    {
        field: 'nakala_sha1', minWidth: 100, cellRenderer: BtnCellRenderer,
        headerName: 'Video',
        cellRendererParams: {
            clicked: function (field, rowData) {
                // // Get modal titlec
                let fr_gloss = rowData.fr_gloss
                let en_gloss = rowData.en_gloss
                // Replace modal title
                let modalTitle = document.querySelector('.modal-title');
                modalTitle.textContent = `${fr_gloss} / ${en_gloss}`;
                // Trigger the modal with correct gif file
                document.getElementById("modal-img").src = `https://api.nakala.fr/data/10.34847/nkl.afb675g5/${field}`;
                { $("#myModal").modal("show"); }
            },
        }
    },
    { field: 'familiarity_mean', headerName: 'Familiarity mean',valueFormatter: params => params.data.familiarity_mean.toFixed(3) },
    { field: 'concreteness_mean', headerName: 'Concreteness mean',valueFormatter: params => params.data.concreteness_mean.toFixed(3)},
    { field: 'iconicity_mean', headerName: 'Iconicity mean', valueFormatter: params => params.data.iconicity_mean.toFixed(3)},
    { field: 'count', headerName: '# Scores'},
];

const gridOptions = {
    columnDefs: columnDefs,
    defaultColDef: {
        flex: 1,
        minWidth: 130,
        filter: 'agNumberColumnFilter',
        sortable: true,
        resizable: true,
    },
    sortingOrder: ['desc', 'asc', null],
    accentedSort: true,
    rowSelection: 'one',
    rowData: null
};



// setup the grid after the page has finished loading
document.addEventListener('DOMContentLoaded', function () {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);
    
    fetch('data/sign_data_website.json')
        .then((response) => response.json())
        .then((data) => gridOptions.api.setRowData(data));

});
