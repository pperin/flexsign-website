# FLexSign: French Sign Language (LSF) lexical database - website demo

This repository contains the data produced for the FLexSign lexical database and a website demo.

## Description

We collected lexical information about the familiarity, concreteness and iconicity of 546 LSF signs. 33 participants from all over France took part in the data collection. FLexSign, the first lexical database for LSF to our knowledge, was created as part of the thesis of Philomène PÉRIN, a PhD student in psycholinguistics, who is working on the neurolinguistic processing of LSF. One aspect of the project also concerns differences in lexical processing and organization according to the age of acquisition and the linguistic profile of signers. The FLexSign database is open to future contributions.


## Local use

1. Download this repository either by downloading and unzipping it from the website or by cloning it with git.

```bash
git clone https://gitlab.huma-num.fr/pperin/flexsign-website.git
cd site
```

2. Launch the site with the command of your choice. For example:

```python
python3 -m http.server
```
3. Open the website using the localhost port assigned. Ex. ```http://localhost:8000/```



## Datatables descriptions

The tsv file ```datatables_descriptions.tsv``` details and describes each header and value of the data tables.

## Descriptive statistics

The repository's Jupyter notebook contains the various descriptive statistics calculated on the raw data.
